softspot
==============

The Python library *softspot* analyzes low-energy quasilocalized excitations in computer glasses, based solely on the harmonic approximation of the potential energy. It accompanies the research paper **A simple and broadly-applicable definition of shear transformation zones** by David Richard, Geert Kapteijns, Julia Giannini, Lisa Manning, and Edan Lerner.

See the `GitLab repository <https://gitlab.com/davricha/softspot>`_ for more information, and the `iPython tutorial <https://gitlab.com/davricha/softspot/-/blob/master/examples/tutorial.ipynb>`_ to get started with a simple example.

.. automodule:: softspot.softspot
   :members:
