# *softspot*, the Python library accompanying the research paper “A simple and broadly-applicable definition of shear transformation zones”

The Python library *softspot* analyzes low-energy quasilocalized excitations in computer glasses, based solely on the harmonic approximation of the potential energy. It accompanies the research paper **A simple and broadly-applicable definition of shear transformation zones** by David Richard, Geert Kapteijns, Julia Giannini, Lisa Manning, and Edan Lerner.

Please refer to the [documentation](https://softspot.readthedocs.io/en/latest/) for an in-depth description of the code.

Please see the [iPython tutorial](https://gitlab.com/davricha/softspot/-/blob/master/examples/tutorial.ipynb) to get started with a simple example.

## Installation

The minimum required Python version is 3.6. Install with
```
pip install softspot
```
